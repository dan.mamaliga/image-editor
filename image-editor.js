"use strict";

const app = {
    offscreenCanvas:null,
    visibleCanvas:null,
    currentAction:null,
    // current selection coordinates
    x1: 0,
    y1: 0,
    x2: 0,
    y2: 0
}

// trigger action for current selection
app.changeSelectionAction = function(action){
    if(action !== app.currentAction){
        app.currentAction = action;
        app.draw();
        const selectionActions = document.getElementById("selectionActions");
        selectionActions.style.display = "none";
    }
}

app.draw = function(){
    // canvas resize
    app.visibleCanvas.width=app.offscreenCanvas.width;
    app.visibleCanvas.height=app.offscreenCanvas.height;

    // apply action on visibleCanvas
    switch(app.currentAction){
        case "normal":
            const vContext = visibleCanvas.getContext("2d");
            vContext.drawImage(app.offscreenCanvas, 0, 0);
            app.currentAction = null;
            break;
        case "invert":
            app.invert();
            break;
        case "red":
            app.red();
            break;
        case "green":
            app.green();
            break;
        case "blue":
            app.blue();
            break;
        case "blur":
            app.blur();
            break;
        case "glitch":
            app.glitch();
            break;
        case "particle":
            app.particle();
            break;
        case "erase":
            app.eraseSelection();
            break;
        case "crop":
            app.crop();
            break;   
    }
}

// erase action
app.eraseSelection = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
            const data = imageData.data;

            for (let i = 0; i < data.length; i += 4) {
                data[i + 3] = 0; //erase
            }
            let minX = app.x1 < app.x2 ? app.x1 : app.x2;
            let minY = app.y1 < app.y2 ? app.y1 : app.y2;
            offscreenContext.putImageData(imageData, minX, minY);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

// crop action
app.crop = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    const canvasWidth = app.x2 - app.x1;
    const canvasHeight = app.y2 - app.y1;
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, canvasWidth, canvasHeight);
    app.offscreenCanvas.width = canvasWidth;
    app.offscreenCanvas.height = canvasHeight;
    offscreenContext.putImageData(imageData, 0, 0);
    app.visibleCanvas.width = canvasWidth;
    app.visibleCanvas.height = canvasHeight;
    visibileContext.putImageData(offscreenContext.getImageData(0,0,
    app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

// blue effect action
app.blue = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
            const data = imageData.data;

            for (let i = 0; i < data.length; i += 4) {
                data[i] = 0; //red
                data[i + 1] = 0; //green
                //data[i + 2] = 0; //blue
            }
            let minX = app.x1 < app.x2 ? app.x1 : app.x2;
            let minY = app.y1 < app.y2 ? app.y1 : app.y2;
            offscreenContext.putImageData(imageData, minX, minY);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

// green effect action
app.green = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
            const data = imageData.data;

            for (let i = 0; i < data.length; i += 4) {
                data[i] = 0; //red
                //data[i + 1] = 0; //green
                data[i + 2] = 0; //blue
            }
            let minX = app.x1 < app.x2 ? app.x1 : app.x2;
            let minY = app.y1 < app.y2 ? app.y1 : app.y2;
            offscreenContext.putImageData(imageData, minX, minY);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

// red effect action
app.red = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
            const data = imageData.data;

            for (let i = 0; i < data.length; i += 4) {
                data[i + 1] = 0; //green
                data[i + 2] = 0; //blue
            }
            let minX = app.x1 < app.x2 ? app.x1 : app.x2;
            let minY = app.y1 < app.y2 ? app.y1 : app.y2;
            offscreenContext.putImageData(imageData, minX, minY);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

// invert colors effect action
app.invert = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
            const data = imageData.data;

        for (let i = 0; i < data.length; i += 4) {
                const r = data[i];
                const g = data[i + 1];
                const b = data[i + 2];

                data[i] = 255 - r;
                data[i + 1] = 255 - g;
                data[i + 2] = 255 - b;
            }
            let minX = app.x1 < app.x2 ? app.x1 : app.x2;
            let minY = app.y1 < app.y2 ? app.y1 : app.y2;
            offscreenContext.putImageData(imageData, minX, minY);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

// blur effect action
app.blur = function (){
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    const imageWidth = app.x2 - app.x1
    const imageHeight = app.y2 - app.y1
    const imageData = offscreenContext.getImageData(app.x1, app.y1, imageWidth, imageHeight);
    const data = imageData.data;
    const blurRate = 15;
    let imageOpacitySum  = 0;
    let imageRedSum = 0;
    let imageGreenSum = 0;
    let imageBlueSum = 0;
    let closePixelsCount = 0;
    let er = 0; // extra red
    let eg = 0; // extra green
    let eb = 0; // extra blue

    for (let br = 0; br < blurRate; br += 1) {
        for (var i = 0, n = data.length; i < n; i += 4) {
            let iMW = 4 * imageWidth;
            imageOpacitySum = imageRedSum = imageGreenSum = imageBlueSum = 0;
            closePixelsCount = 0;
            // data of close pixels (from all 8 surrounding pixels)
            let closePixelsData = [
                i - iMW - 4, i - iMW, i - iMW + 4, // top pixels
                i - 4, i + 4, // middle pixels
                i + iMW - 4, i + iMW, i + iMW + 4 // bottom pixels
            ];
            // calculating Sum value of all close pixels
            for (let e = 0; e < closePixelsData.length; e += 1) {
                if (closePixelsData[e] >= 0 && closePixelsData[e] <= data.length - 3) {
                    imageOpacitySum += data[closePixelsData[e]];
                    imageRedSum += data[closePixelsData[e] + 1];
                    imageGreenSum += data[closePixelsData[e] + 2];
                    imageBlueSum += data[closePixelsData[e] + 3];
                    closePixelsCount += 1;
                }
            }
            // apply average values
            data[i] = (imageOpacitySum / closePixelsCount) + er;
            data[i+1] = (imageRedSum / closePixelsCount) + eg;
            data[i+2] = (imageGreenSum / closePixelsCount) + eb;
            data[i+3] = (imageBlueSum / closePixelsCount);
        }
    }
    let minX = app.x1 < app.x2 ? app.x1 : app.x2;
    let minY = app.y1 < app.y2 ? app.y1 : app.y2;
    offscreenContext.putImageData(imageData, minX, minY);
    visibileContext.putImageData(offscreenContext.getImageData(0,0,
        app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
app.currentAction = null;
}

// glitch effect action
app.glitch = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
            const data = imageData.data;
            const rOffset = 20;
            const gOffset = -10;
            const bOffset = 20;
            const newData = new Uint8ClampedArray(data);
            for (let i = 0; i < data.length; i += 4) {
                const r = data[i];//red
                const g = data[i + 1];//green
                const b = data[i + 2];//blue
                
                newData[i + rOffset * 4] = r; 
                newData[i + 1 + gOffset * 4] = g; 
                newData[i + 2 + bOffset * 4] = b; 
            }
            let minX = app.x1 < app.x2 ? app.x1 : app.x2;
            let minY = app.y1 < app.y2 ? app.y1 : app.y2;
            offscreenContext.putImageData(new ImageData(newData, imageData.width, imageData.height), minX, minY);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null
}

// particle effect action
app.particle = function () {
    const offscreenContext = app.offscreenCanvas.getContext("2d");
    const visibileContext = app.visibleCanvas.getContext("2d");
    // get current selection pixels
    const imageData = offscreenContext.getImageData(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
        let pixels=[];
        let pos=0;
        const data=imageData.data;
        let minX = app.x1 < app.x2 ? app.x1 : app.x2;
        let minY = app.y1 < app.y2 ? app.y1 : app.y2;
        //iterate through image pixels
        for(let i=1;i<=imageData.height;i++){
            for(let j=1;j<=imageData.width;j++){
                pos=[(i-1)*imageData.width+(j-1)]*4; //Get the pixel position
                if(data[pos]>=0){
                    let pixel={
                        x:minX + j + Math.random()*20, //Reset the location information for each pixel
                        y:minY + i + Math.random()*20, //Reset the location information for each pixel
                        fillStyle:'rgba('+data[pos]+','+(data[pos+1])+','+(data[pos+2])+','+(data[pos+3])+')'
                    }
                    pixels.push(pixel);
                }

            }
        }
        const len=pixels.length;
        let currentPixel=null;
        // draw pixels as particles
        for(let i=0;i<len;i++){
            currentPixel=pixels[i];
            offscreenContext.fillStyle=currentPixel.fillStyle;
            offscreenContext.fillRect(currentPixel.x,currentPixel.y,1,1);
        }
        visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0);
    app.currentAction = null;
}

app.load = function(){

    app.visibleCanvas = document.getElementById("visibleCanvas");
    app.offscreenCanvas = document.createElement("canvas");
    
    // initial selection(all image)
    document.getElementById("btnSelect").addEventListener("click",()=>{
        const selectionActions = document.getElementById("selectionActions");
        if (selectionActions.style.display === "none")
            selectionActions.style.display = "block";
        const visibileContext = app.visibleCanvas.getContext("2d");
        const offscreenContext = app.offscreenCanvas.getContext("2d");
        app.x1 = app.y1 = 0;
        app.x2 = app.offscreenCanvas.width;
        app.y2 = app.offscreenCanvas.height;
        let offset = 0;
        // draw ant stroke on initial selection
        function march() {
            offset++;
            if (offset > 16) {
              offset = 0;
            }
            visibileContext.setLineDash([6, 4]);
            visibileContext.lineDashOffset = -offset;
            visibileContext.lineWidth = 6;
            visibileContext.strokeRect(0,0,app.visibleCanvas.width, app.visibleCanvas.height);
          }
          march();
          // save current selection coordinates and draw ant stroke
          app.visibleCanvas.addEventListener("mousedown", function(ev){
               app.x1 = ev.offsetX;
               app.y1 = ev.offsetY;
            console.log("Mouse down coordinates: " + app.x1 + "," + app.y1);
          });
          app.visibleCanvas.addEventListener("mouseup", function(ev){
            app.x2 = ev.offsetX;
            app.y2 = ev.offsetY;
            console.log("Mouse up coordinates: " + app.x2 + "," + app.y2);
            visibileContext.putImageData(offscreenContext.getImageData(0,0,
                app.offscreenCanvas.width, app.offscreenCanvas.height),0,0); 
            visibileContext.strokeRect(app.x1, app.y1, app.x2 - app.x1, app.y2 - app.y1);
          console.log("Selection coordinates: " + app.x1 + "," + app.y1 + "," + app.x2 + "," + app.y2);
            });
    });

    // display/hide text form
    document.getElementById("btnAddText").addEventListener("click",()=>{
        const customTextForm = document.getElementById("customTextForm");
        if(customTextForm.style.display === "none")
        customTextForm.style.display = "block";
        else
        customTextForm.style.display = "none";
    });

    // add text on double click on image
    app.visibleCanvas.addEventListener("dblclick",function(ev){
        let x = ev.offsetX;
        let y = ev.offsetY;
        const offscreenContext = app.offscreenCanvas.getContext("2d");
        const visibileContext = app.visibleCanvas.getContext("2d");
        const customTextValue = document.getElementById("customText").value;
        const customTextSize = document.getElementById("textSize").value;
        const customTextColor = document.getElementById("textColor").value;
        if(customTextValue !== ''){
        console.log("Text attributes: " + customTextValue + "," + customTextSize + "," + customTextColor);
        offscreenContext.font = customTextSize + "pt Calibri";
        offscreenContext.fillStyle = customTextColor;
        offscreenContext.fillText(customTextValue, x, y);
        visibileContext.drawImage(app.offscreenCanvas, 0, 0);
        const customTextForm = document.getElementById("customTextForm");
        customTextForm.style.display = "none";
        }
    });

    // download image
    document.getElementById("btnDownload").addEventListener("click",()=>{
        const dataUrl=app.visibleCanvas.toDataURL();
        const link=document.createElement('a');
        link.download='image.png';
        link.href=dataUrl;
        link.click();
    });

    // listen when action is selected
    const actionButtons = document.querySelectorAll("button[data-selection-action]");
    for(let i=0; i<actionButtons.length; i++){
        actionButtons[i].addEventListener("click", function(ev){
            const action = ev.target.dataset.selectionAction;
            app.changeSelectionAction(action);
        })
    }

    // load image to browser
    document.getElementById("fileBrowser").addEventListener("change", function(ev){
        const files = ev.target.files;

        const reader = new FileReader();
        reader.addEventListener("load", function(ev){
            const dataUrl = ev.target.result;

            const img = document.createElement("img");
            img.addEventListener("load", function(ev){              
                // canvas resize
                app.offscreenCanvas.width=img.naturalWidth;
                app.offscreenCanvas.height=img.naturalHeight;
                //draw image on offscreenCanvas
                const oContext = app.offscreenCanvas.getContext("2d");
                oContext.drawImage(ev.target, 0, 0);             
                console.log("Here...")
                //draw image on visibleCanvas
                app.currentAction = "normal";
                app.draw();
            });
            img.src = dataUrl;
        });
        reader.readAsDataURL(files[0]);
    });
}